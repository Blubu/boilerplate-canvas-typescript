import {MainLoop} from "./MainLoop";

//OOP VERSION
class index {
    private gameContainer: HTMLElement = document.getElementById('canvas-container');
    private canvas: HTMLCanvasElement;

    public main() : void {
        this.canvas = document.createElement('canvas');

        let w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByTagName('body')[0],
            x = w.innerWidth || e.clientWidth || g.clientWidth,
            y = w.innerHeight|| e.clientHeight|| g.clientHeight;

        this.canvas.width = x;
        this.canvas.height = y;
        this.canvas.tabIndex = 1;

        this.gameContainer.appendChild(this.canvas);
        this.canvas.focus();

        let mainLoop = new MainLoop(this.canvas , this.canvas.width, this.canvas.height);
        // mainGame.setGameContainer(this.canvas);
        // mainGame.runGame();
        mainLoop.start();
    }
}

let game = new index();
game.main();
console.log("hallo");