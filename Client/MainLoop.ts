import {controlsService} from "./Services/Controls.service";

export class MainLoop {
    private canvas: HTMLCanvasElement;
    private ctx: CanvasRenderingContext2D;
    private width: number;
    private height: number;

    private lastCalled: number;
    private fps: number;


    constructor(canvas: HTMLCanvasElement, width: number, height: number) {
        this.canvas = canvas;
        this.ctx = canvas.getContext('2d');
        this.width = width;
        this.height = height;
        controlsService.setCanvas(canvas);
    }

    start(): void {
        //do stuff like loading from file-system etc. here before starting main loop

        this.render();
    }

    update(): void {}

    render(): void {
        this.update();

        this.calcFps();
        requestAnimationFrame(() => {
            this.render();
        })
    }

    private calcFps() {
        if(!this.lastCalled) {
            this.lastCalled = performance.now();
            this.fps = 0;
            return;
        }

        let delta = (performance.now() - this.lastCalled)/1000;
        this.lastCalled = performance.now();
        this.fps = Math.floor(1/delta);
    }

}