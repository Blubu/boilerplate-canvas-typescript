export class ControlsService {
    public keys: Map<number, boolean> = new Map<number, boolean>();

    setCanvas(canvas: HTMLCanvasElement): void {
        canvas.addEventListener('keydown', this.onKeyDown.bind(this), true);
        canvas.addEventListener('keyup', this.onKeyUp.bind(this), true);
    }

    onKeyDown(event: KeyboardEvent): void {
        this.keys.set(event.keyCode, true);
        console.log(event.keyCode, "pressed");
    }

    onKeyUp(event: KeyboardEvent): void {
        this.keys.set(event.keyCode, false);
    }

}

export let controlsService = new ControlsService();