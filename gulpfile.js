let gulp = require('gulp');
let webpack = require('webpack');

gulp.task('frontend', function (done) {
    let clientWebpackConfig = require('./webpack.config');
    webpack(clientWebpackConfig).run(function (err, stats) {
        if(err) {
            console.log('ERROR', err);
        } else {
            console.log(stats.toString());
        }
        done();
    })
});

gulp.task('copy', function () {
    gulp.src('./Client/index.html')
        .pipe(gulp.dest('./dist'));
});

gulp.task('build', ['frontend', 'copy']);